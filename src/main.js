import Vue from 'vue'
import App from './App.vue'

Vue.config.productionTip = false;

alert("The bundle was created with " + process.env.VUE_APP_SECRET);

new Vue({
  render: h => h(App),
}).$mount('#app')
